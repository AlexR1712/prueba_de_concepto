# El Juego de La Vida

Trabajo en Equipo 

## Introducción

El juego de la vida es un autómata celular diseñado por el matemático británico John Horton Conway en 1970.

Hizo su primera aparición pública en el número de octubre de 1970 de la revista Scientific American, en la columna de juegos matemáticos de Martin Gardner. Desde un punto de vista teórico, es interesante porque es equivalente a una máquina universal de Turing, es decir, todo lo que se puede computar algorítmicamente se puede computar en el juego de la vida.

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

#Variantes del Juego de la Vida
 Tomando en cuenta que el **Juego de la Vida de Conway** esta basado en una serie de reglas que describen cambios poblacionales a lo largo de generaciones. Algunas de las variantes que se podrian mencionar son: 
  1. 1357/1357 (crece) todo son replicantes
  2. 1358/357 (caótico) un reino equilibrado de amebas
  3. 23/3 (complejo) "Juego de la Vida de Conway"
  4. 4/2 (crece) generador de patrones de alfombras

 #Relaciones entre el Juego de la Vida y posibles aplicaciones

El juego de la vida es un autómata celular diseñado por el matemático británico John Horton Conway en 1970.

Hizo su primera aparición pública en el número de octubre de 1970 de la revista Scientific American, en la columna de juegos matemáticos de Martin Gardner. Desde un punto de vista teórico, es interesante porque es equivalente a una máquina universal de Turing, es decir, todo lo que se puede computar algorítmicamente se puede computar en el juego de la vida.

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Nombres: 

- ERICK0 EL DE ABAJO COMO MOCO
- Mariangel y Brandon estuvieron aqui
- emiles estuvo aqui :D
- Diego Karabin (Benevolente Dictador de Por Vida)